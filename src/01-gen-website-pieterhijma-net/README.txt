It takes a long time, the output should more or less be this:

opt:verbose:1
dispatch:install,name=top
proc_cmd:
cmd_install:
get_opt(variant,0)=(null)
get_opt(uri,0)=(null)
sbcl_version_bin
ensure_directories_exist:/home/pieter/.roswell/tmp/sbcl-bin_uri.tsv
System:mkdir -p /home/pieter/.roswell/tmp/
No SBCL version specified. Downloading sbcl-bin_uri.tsv to see the available versions...
get_opt(sbcl-bin-version-uri,0)=(null)
get_opt(ros.proxy,1)=(null)
get_opt(proxy.http.only,0)=(null)
get_opt(ssl.ignore_verify,0)=(null)
http response:302
http response:200
[##########################################################################]100%
System:mv /home/pieter/.roswell/tmp/sbcl-bin_uri.tsv.partial /home/pieter/.roswell/tmp/sbcl-bin_uri.tsv
uname=linux uname-m=x86-64
open /home/pieter/.roswell/tmp/sbcl-bin_uri.tsv
header os=0,arch=1,version=2,variant=3,uri=4
1 os:freebsd arch:arm64 variant:(null) version:2.4.0
2 os:netbsd arch:arm64 variant:(null) version:2.4.0
3 os:openbsd arch:arm64 variant:(null) version:2.4.0
4 os:freebsd arch:x86 variant:(null) version:2.4.0
5 os:darwin arch:x86-64 variant:(null) version:2.4.0
6 os:linux arch:arm64 variant:musl version:2.4.0
7 os:linux arch:arm64 variant:(null) version:2.4.0
8 os:linux arch:armhf variant:(null) version:2.4.0
9 os:linux arch:x86 variant:glibc2.31 version:2.4.0
10 os:linux arch:x86-64 variant:glibc2.31 version:2.4.0
Installing sbcl-bin/2.4.0...
ensure_directories_exist:/home/pieter/.roswell/local-projects/
System:mkdir -p /home/pieter/.roswell/local-projects/
directory_exist_p(/home/pieter/.roswell/impls/x86-64/linux/sbcl-bin/2.4.0/)=0
ensure_directories_exist:/home/pieter/.roswell/tmp/sbcl-bin-2.4.0/
System:mkdir -p /home/pieter/.roswell/tmp/sbcl-bin-2.4.0/
/home/pieter/.roswell/tmp/sbcl-bin-2.4.0.lock
System:touch /home/pieter/.roswell/tmp/sbcl-bin-2.4.0.lock
get_opt(uri,0)=(null)
sbcl_bin_download
Downloading https://github.com/roswell/sbcl_bin/releases/download/2.4.0/sbcl-2.4.0-x86-64-linux-binary.tar.bz2
ensure_directories_exist:/home/pieter/.roswell/archives/sbcl-bin-2.4.0-x86-64-linux.tar.bz2
System:mkdir -p /home/pieter/.roswell/archives/
System:rm -f /home/pieter/.roswell/tmp/sbcl-bin-2.4.0.lock
get_opt(ros.proxy,1)=(null)
get_opt(proxy.http.only,0)=(null)
get_opt(ssl.ignore_verify,0)=(null)
http response:302
http response:200
[##########################################################################]100%
System:mv /home/pieter/.roswell/archives/sbcl-bin-2.4.0-x86-64-linux.tar.bz2.partial /home/pieter/.roswell/archives/sbcl-bin-2.4.0-x86-64-linux.tar.bz2
sbcl_bin_expand
Extracting sbcl-bin-2.4.0-x86-64-linux.tar.bz2 to /home/pieter/.roswell/src/sbcl-2.4.0-x86-64-linux/
System:rm -rf /home/pieter/.roswell/src/sbcl-2.4.0-x86-64-linux/
ensure_directories_exist:/home/pieter/.roswell/src/sbcl-2.4.0-x86-64-linux/
System:mkdir -p /home/pieter/.roswell/src/sbcl-2.4.0-x86-64-linux/
System:rm -f /home/pieter/.roswell/tmp/sbcl-bin-2.4.0.lock
cmd_tar:5
extracttype=bzip2
which cmd:command -v "gtar"
which result:
extractcmd=bzip2 -dc /home/pieter/.roswell/archives/sbcl-bin-2.4.0-x86-64-linux.tar.bz2 | tar -xf - -C /home/pieter/.roswell/src/
System:bzip2 -dc /home/pieter/.roswell/archives/sbcl-bin-2.4.0-x86-64-linux.tar.bz2 | tar -xf - -C /home/pieter/.roswell/src/
Building sbcl-bin/2.4.0...ensure_directories_exist:/home/pieter/.roswell/impls/x86-64/linux/sbcl-bin/2.4.0
System:mkdir -p /home/pieter/.roswell/impls/x86-64/linux/sbcl-bin/
System:rm -f /home/pieter/.roswell/tmp/sbcl-bin-2.4.0.lock
ensure_directories_exist:/home/pieter/.roswell/impls/log/sbcl-bin-2.4.0/install.log
System:mkdir -p /home/pieter/.roswell/impls/log/sbcl-bin-2.4.0/
System:rm -f /home/pieter/.roswell/tmp/sbcl-bin-2.4.0.lock
change_directory:/home/pieter/.roswell/src/sbcl-2.4.0-x86-64-linux/
System:(cat find-gnumake.sh; echo find_gnumake)|sh
 Done.
which cmd:command -v "ros"
which result:/usr/bin/ros

which cmd:command -v "patchelf"
which result:
'patchelf' not detected.no patching for sbcl
impl sbcl-bin version= 2.4.0 
set_opt(default.lisp)='sbcl-bin'
set_opt(sbcl-bin.version)='2.4.0'
done with install impl 
/etc/roswell/install.ros 
sbcl-bin has not been implemented internally yet. /etc/roswell/install.ros argc:2
install:sbcl-bin:
argc_=3 argv[0]=--,argv[1]=/etc/roswell/install.ros,argv[2]=sbcl-bin,dispatch:--,name=top
proc_options:--:top
cmd_script_frontend:0
frontend:script_:argc=2 argv[0]=/etc/roswell/install.ros
ros_script_cmd=exec ros -Q +R -L sbcl-bin -- $0 "$@"
dispatch:-Q,name=top
proc_options:-Q:top
opt:quicklisp:2
dispatch:+R,name=top
proc_options:+R:top
opt:no-rc:0
dispatch:-L,name=top
proc_options:-L:top
get_opt(lisp,0)=(null)
take1:lisp:sbcl-bin,(null)
set_opt(lisp)='sbcl-bin'
dispatch:--,name=top
proc_options:--:top
cmd_script_frontend:1
cmd_script
get_opt(program,0)=(null)
script_:argc=3 argv[0]=--
current=(null)
set_opt(script)='"/etc/roswell/install.ros""sbcl-bin"'
cmd_run_star:1:argv[0],script
get_opt(image,0)=(null)
get_opt(roswellenv,0)=(null)
set_env_opt:.roswellenv
get_opt(roswellenv,1)=(null)
set_env_opt:/home/pieter/.roswell/env/
get_opt(lisp,1)=sbcl-bin
determin_impl:sbcl-bin
get_opt(sbcl-bin.version,1)=2.4.0
set_opt(impl)='sbcl-bin/2.4.0'
get_opt(quicklisp,0)=(null)
set_opt(quicklisp)='/home/pieter/.roswell/lisp/quicklisp/'
set_opt(argv0)='ros'
which cmd:command -v "ros"
which result:/usr/bin/ros

set_opt(wargv0)='/usr/bin/ros'
set_opt(homedir)='/home/pieter/.roswell/'
set_opt(verbose)='1'
set_opt(lispdir)='/etc/roswell/'
set_opt(patchdir)='/etc/roswell/patch/'
get_opt(asdf.version,0)=(null)
set_opt(uname)='linux'
set_opt(uname-m)='x86-64'
get_opt(program,0)=(null)
get_opt(asdf.version,0)=(null)
set_opt(program)='(:eval"(ros:quicklisp)")'
get_opt(impl,0)=sbcl-bin/2.4.0
get_opt(help,0)=(null)
get_opt(script,0)="/etc/roswell/install.ros""sbcl-bin"
get_opt(image,0)=(null)
get_opt(program,0)=(:eval"(ros:quicklisp)")
get_opt(dynamic-space-size,1)=(null)
get_opt(control-stack-size,1)=(null)
get_opt(without-roswell,0)=(null)
get_opt(enable-debugger,0)=(null)
get_opt(version,0)=(null)
get_opt(asdf.version,0)=(null)
init.lisp=/etc/roswell/init.lisp

help=nil script="/etc/roswell/install.ros""sbcl-bin"
get_opt(wrap,1)=(null)
args /home/pieter/.roswell/impls/x86-64/linux/sbcl-bin/2.4.0/bin/sbcl --core /home/pieter/.roswell/impls/x86-64/linux/sbcl-bin/2.4.0/lib/sbcl/sbcl.core --noinform --no-sysinit --no-userinit --disable-debugger --eval (progn #-ros.init(cl:load "/etc/roswell/init.lisp")) --eval (ros:run '((:eval"(ros:quicklisp)")(:script "/etc/roswell/install.ros""sbcl-bin")(:quit ()))) 
ROS_OPTS (("program""(:eval\"(ros:quicklisp)\")")("uname-m""x86-64")("uname""linux")("patchdir""/etc/roswell/patch/")("lispdir""/etc/roswell/")("verbose""1")("homedir""/home/pieter/.roswell/")("wargv0""/usr/bin/ros")("argv0""ros")("quicklisp""/home/pieter/.roswell/lisp/quicklisp/")("impl""sbcl-bin/2.4.0")("script""\"/etc/roswell/install.ros\"\"sbcl-bin\"")("lisp""sbcl-bin")("sbcl-bin.version""2.4.0")("default.lisp""sbcl-bin"))
LOAD /etc/roswell/util-install.lisp
LOAD /etc/roswell/locations.lisp
LOAD /etc/roswell/util.lisp
LOAD /etc/roswell/install-sbcl-bin.lisp
:<install SBCL-BIN-ARGV-PARSE
(:TARGET "sbcl-bin" :VERSION NIL :VERSION-NOT-SPECIFIED NIL :ARGV NIL)
:>
Install Script for sbcl-bin...
:<install SETUP
(:TARGET "sbcl-bin" :VERSION NIL :VERSION-NOT-SPECIFIED NIL :ARGV NIL)
:>Making core for Roswell...
Installing Quicklisp... Done 6766
